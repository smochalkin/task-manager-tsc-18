package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;

public class UserLogoutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Log out.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

}
