package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserUpdateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-update";
    }

    @Override
    public String description() {
        return "Update a user.";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter first name:");
        String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().userUpdate(userId, firstName, lastName, middleName);
    }

}
