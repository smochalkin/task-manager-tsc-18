package ru.smochalkin.tm.command.system;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;

import java.util.Collection;

public class HelpCommand extends AbstractSystemCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command);
        }
    }

}
