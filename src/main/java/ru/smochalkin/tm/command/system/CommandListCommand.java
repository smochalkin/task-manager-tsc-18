package ru.smochalkin.tm.command.system;

import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.model.Command;

import java.util.List;

public class CommandListCommand extends AbstractSystemCommand {

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Display list of commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        List<String> keys = serviceLocator.getCommandService().getCommandNames();
        for (String value : keys) {
            if (value == null || value.isEmpty()) continue;
            System.out.println(value);
        }
    }

}
