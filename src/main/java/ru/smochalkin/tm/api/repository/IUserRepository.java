package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    User findById(String id);

    User removeUser(User user);

    User findByLogin(String login);

    User removeById(String id);

    User removeByLogin(String login);

    Boolean isLogin(String login);
}
