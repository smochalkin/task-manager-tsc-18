package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String desc);

    Task updateByIndex(Integer index, String name, String desc);

    int getCount();

    Task updateStatusById(String id, Status status);

    Task updateStatusByName(String name, Status status);

    Task updateStatusByIndex(int index, Status status);

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String taskId);

    List<Task> findTasksByProjectId(String projectId);

    void removeTasksByProjectId(String projectId);

}
