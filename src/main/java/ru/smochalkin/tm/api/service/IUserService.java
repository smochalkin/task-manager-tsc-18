package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.model.User;

import java.util.List;

public interface IUserService {

    User findById(String id);

    User findByLogin(String login);

    User removeById(String id);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User userUpdate(String userId, String firstName, String lastName, String middleName);

    boolean isLogin(String login);

}
