package ru.smochalkin.tm.model;

import ru.smochalkin.tm.api.model.IWBS;
import ru.smochalkin.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Task implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private String projectId;

    private Status status = Status.NOT_STARTED;

    private final Date created = new Date();

    private Date startDate;

    private Date endDate;

    public Task() {
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return id + ": " + name + ": " + description +
                "; Status - " + status.getDisplayName() +
                "; Created - " + created +
                "; Start - " + startDate +
                "; End - " + endDate;
    }

}
