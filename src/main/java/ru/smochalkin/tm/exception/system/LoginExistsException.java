package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Warning! Login already exists...");
    }

}
