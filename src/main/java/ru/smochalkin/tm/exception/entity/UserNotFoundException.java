package ru.smochalkin.tm.exception.entity;

import ru.smochalkin.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
