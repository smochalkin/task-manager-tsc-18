package ru.smochalkin.tm.exception.empty;

import ru.smochalkin.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
