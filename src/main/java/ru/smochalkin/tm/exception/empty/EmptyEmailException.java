package ru.smochalkin.tm.exception.empty;

import ru.smochalkin.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}
