package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public User add(User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(String id) {
        for (User user : users) {
            if (id.equals(user.getId())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User removeUser(User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User findByLogin(String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User removeById(String id) {
        User user = findById(id);
        return removeUser(user);
    }

    @Override
    public User removeByLogin(String login) {
        User user = findByLogin(login);
        return removeUser(user);
    }

    @Override
    public Boolean isLogin(String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

}
