package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.system.IndexIncorrectException;
import ru.smochalkin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        return projectRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(Integer index) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(name);
    }

    @Override
    public Project removeByIndex(Integer index) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateById(String id, String name, String desc) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.updateById(id, name, desc);
    }

    @Override
    public Project updateByIndex(Integer index, String name, String desc) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.updateByIndex(index, name, desc);
    }

    @Override
    public Project updateStatusById(String id, Status status) {
        return projectRepository.updateStatusById(id, status);
    }

    @Override
    public Project updateStatusByName(String name, Status status) {
        return projectRepository.updateStatusByName(name, status);
    }

    @Override
    public Project updateStatusByIndex(Integer index, Status status) {
        return projectRepository.updateStatusByIndex(index, status);
    }

    @Override
    public boolean isIndex(Integer index) {
        if (index == null || index < 0) return false;
        if (index >= projectRepository.getCount()) return false;
        return true;
    }

}
